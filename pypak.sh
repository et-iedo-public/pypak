#!/usr/bin/env bash
TARGET_PROGRAM=$1
PROGRAM_NAME=$( basename ${TARGET_PROGRAM} )
STARTDIR="$( cd "$(dirname "$0")" ; pwd -P )"
PROJECT=pypak
WORKDIR=$(mktemp -d -t ${PROJECT}-XXXXXXXXXX)
PROGRAMDIR="$( cd "$(dirname "${TARGET_PROGRAM}")" ; pwd -P )"
cp ${PROGRAMDIR}/requirements.txt ${WORKDIR}/
cp ${TARGET_PROGRAM} ${WORKDIR}/

echo Entering temp dir...
echo ${WORKDIR}
cd ${WORKDIR}

pip3.7 install virtualenv
VENV=$(which virtualenv)

if [ ! -f "$VENV" ]; then
	echo "virtualenv command not found, please install: pip install virtualenv"
	exit 1
fi

PACKAGES=$( egrep -o '^import .*|^from .*import ' ${WORKDIR}/${PROGRAM_NAME} \
	| sed 's/^from//' | sed 's/import //' | sed 's/,//g' | awk '{printf $0}'
)
SKIP_PACKAGES=(sys os socket logging json re urllib.request io)
echo Packages detected are: ${PACKAGES}

# For the main suparenting.py
${VENV} -p python3 env
source env/bin/activate
pip install --upgrade pip
pip install --upgrade setuptools
shift
[[ $@ ]] && pip install $@
for package in ${PACKAGES}
do
   [[ ! " ${SKIP_PACKAGES[@]} " =~ " ${package} " ]] && pip install -q ${package}
done
pip install ipython # I always use it inline import in the -i option section
pip install -r requirements.txt
cp requirements.txt requirements.txt.orig

${WORKDIR}/${PROGRAM_NAME} -h
statuscode=$?
if [ "$statuscode" -eq "0" ]; then
   pip freeze > requirements.txt
   cp requirements.txt ${PROGRAMDIR}/
   echo requirements.txt updated:
   diff requirements.txt{.orig,}
   cd ${PROGRAMDIR}
   rm -rf ${WORKDIR}
fi 
deactivate
